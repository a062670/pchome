const axios = require("axios");
const moment = require("moment");
const fs = require("fs");
const iconv = require("iconv-lite");
const jschardet = require("jschardet");
const opn = require("opn");
const qs = require("qs");

let url = "https://24h.pchome.com.tw/fscart/index.php/prod/modify";
let snapupUrl =
  "https://24h.pchome.com.tw/prod/cart/v1/prod/DBBR05-A900AMA8Y-000/snapup";

let cookie = fs.readFileSync("cookie.txt");
let cookieEncoding = jschardet.detect(cookie).encoding;
cookie = iconv.decode(cookie, cookieEncoding);
cookie = cookie.replace("\n", "").replace("\r", "");

let item = fs.readFileSync("item.txt");
let itemEncoding = jschardet.detect(item).encoding;
item = iconv.decode(item, itemEncoding);
item = item.replace("\n", "").replace("\r", "");

let rs = fs.readFileSync("rs.txt");
let rsEncoding = jschardet.detect(rs).encoding;
rs = iconv.decode(rs, rsEncoding);
rs = rs.replace("\n", "").replace("\r", "");

let MACExpire = "";
let MAC = "";

snapup();

function snapup() {
  console.log("snapup", moment().format("HH:mm"));
  axios({
    method: "get",
    url: snapupUrl,
    params: {},
    data: "",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      origin: "https://24h.pchome.com.tw",
      Cookie: cookie,
      "user-agent":
        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
      referer: "https://24h.pchome.com.tw/prod/" + item
    }
  })
    .then(response => {
      console.log("snapup resp", JSON.stringify(response.data));
      if (response.data && response.data.MAC && response.data.MACExpire) {
        MACExpire = response.data.MACExpire;
        MAC = response.data.MAC;
        check();
      } else {
        setTimeout(snapup, 500);
      }
    })
    .catch(error => {
      setTimeout(snapup, 500);
    });
}

function check() {
  console.log("modify", moment().format("HH:mm"));
  axios({
    method: "post",
    url: url,
    params: {},
    data: qs.stringify({
      data: `{"G":[],"A":[],"B":[],"TB":"24H","TP":2,"T":"ADD","TI":"${item}-000","RS":"${rs}","YTQ":1,"CAX":"${MAC}","CAXE":"${MACExpire}"}`
    }),
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
      origin: "https://24h.pchome.com.tw",
      Cookie: cookie,
      "user-agent":
        "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36",
      referer: "https://24h.pchome.com.tw/prod/" + item
    }
  })
    .then(response => {
      console.log(JSON.stringify(response.data));
      if (response.data.PRODADD !== "0") {
        opn(
          "https://ecssl.pchome.com.tw/sys/cflowex/fsindex/BigCar/BIGCAR/ItemList"
        );
      } else {
        setTimeout(check, 500);
      }
    })
    .catch(error => {
      setTimeout(check, 500);
    });
}
